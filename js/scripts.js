jQuery.noConflict();
jQuery(document).ready(function(){
	
	// mod_newsflash_xart
	jQuery('.mod_newsflash_xart .mod-title').append('<a class="prev"/><a class="next"/>');
	jQuery('.mod_newsflash_xart section').wrap('<div class="scrollable"/>');
	jQuery('.mod_newsflash_xart .scrollable').scrollable({circular:true,prev:'.mod_newsflash_xart .prev',next:'.mod_newsflash_xart .next'}).autoscroll({interval:5000});

	//mod_custom-pozvanky
	jQuery('.mod_custom-pozvanky').append('<a class="prev"/><a class="next"/>');
	jQuery('.mod_custom-pozvanky .wrapper').wrap('<div class="scrollable"/>');
    if (jQuery.browser.msie && parseFloat(jQuery.browser.version) < 10){
        jQuery('').addClass('pie');
        var addHeight = 45;
    } else {
    	var addHeight = 30;
    }
	var heightArray = jQuery('.mod_custom-pozvanky ul').map( function(){
	   	return  jQuery(this).height();
   	}).get();
	var maxHeight = Math.max.apply( Math, heightArray);
	jQuery('.mod_custom-pozvanky ul').height(maxHeight);
	jQuery('.mod_custom-pozvanky .scrollable').height(maxHeight+addHeight);
	jQuery('.mod_custom-pozvanky .scrollable').scrollable({circular:true,prev:'.mod_custom-pozvanky .prev',next:'.mod_custom-pozvanky .next',vertical:true});

	// mod_banners
	jQuery('.mod_banners').prepend('<a class="close">&times;</a>');
	jQuery('#col-bottom .in').prepend('<a class="mod_banners-button"/>');
	jQuery('#col-bottom .mod_banners-button').click(function(){
		jQuery('.mod_banners').addClass('open');
		jQuery(this).hide();
	});
	jQuery('.mod_banners .mod-title span,.mod_banners .close').click(function(){
		jQuery('.mod_banners').removeClass('open');
		jQuery('#col-bottom .mod_banners-button').show();
	});
	jQuery('#col-bottom .mod_banners-button').hover(function(){
		jQuery('.mod_banners').toggleClass('hover');
	});
	//.custom-banner mark
	jQuery('.custom-banner a').hover(function(){
		jQuery('mark',this).stop(true,true).slideToggle(100);
	});

	// file input style
	jQuery('form.normal input[type=file]').each(function(){
		var userLang = (navigator.language) ? navigator.language : navigator.userLanguage; 
		var uploadtext = 'Choose file';
		if (userLang == 'cs'){var uploadText = 'Vyberte soubor';}
		if (userLang == 'de'){var uploadText = 'Datei ausw&auml;hlen';}
		if (userLang == 'pl'){var uploadText = 'Wybierz plik';}
		if (userLang == 'fr'){var uploadText = 'Choisir un fichier';}
		if (userLang == 'ru'){var uploadText = 'Выберите файл';}
		if (userLang == 'es'){var uploadText = 'Elegir archivo';}
		var uploadbutton = '<input type="button" class="normal-button" value="'+uploadText+'" />';
		var inputClass = jQuery(this).attr('class');
		jQuery(this).wrap('<span class="fileinputs"></span>');
		jQuery(this).parent().append(jQuery('<span class="fakefile" />').append(jQuery('<input class="input-file-text" type="text" />').attr({'id':jQuery(this).attr('id')+'__fake','class':jQuery(this).attr('class')+' input-file-text'})).append(uploadbutton));
		
		jQuery(this).addClass('type-file').css('opacity',0);
		jQuery(this).bind('change mouseout', function() {
			jQuery('#'+jQuery(this).attr('id')+'__fake').val(jQuery(this).val().replace(/^.+\\([^\\]*)$/,'$1'));
		});
	});

	// ie6 condition 
    if (jQuery.browser.msie && parseFloat(jQuery.browser.version) < 7){
    	// mod_mainmenu
        jQuery('.mod_mainmenu li').hover(function(){
        	jQuery(this).toggleClass('hover');
        });
        jQuery('input[type="text"]').addClass('type-text');
        jQuery('input[type="password"]').addClass('type-password');
        jQuery('input[type="submit"]').addClass('type-submit');
        jQuery('#col-bottom').addClass('pngfix');
    }
    
    // ie6-8 condition 
    if (jQuery.browser.msie && parseFloat(jQuery.browser.version) < 9){
        jQuery('').addClass('pie');
    }
    
    
	// detect browser   
	var a=navigator.userAgent.toLowerCase();jQuery.browser.chrome=/chrome/.test(navigator.userAgent.toLowerCase());if(jQuery.browser.msie){jQuery('html').addClass('browserIE');jQuery('html').addClass('browserIE'+jQuery.browser.version.substring(0,1))}if(jQuery.browser.chrome){jQuery('html').addClass('browserChrome');a=a.substring(a.indexOf('chrome/')+7);a=a.substring(0,1);jQuery('html').addClass('browserChrome'+a);jQuery.browser.safari=false}if(jQuery.browser.safari){jQuery('html').addClass('browserSafari');a=a.substring(a.indexOf('version/')+8);a=a.substring(0,1);jQuery('html').addClass('browserSafari'+a)}if(jQuery.browser.mozilla){if(navigator.userAgent.toLowerCase().indexOf('firefox')!=-1){jQuery('html').addClass('browserFirefox');a=a.substring(a.indexOf('firefox/')+8);a=a.substring(0,1);jQuery('html').addClass('browserFirefox'+a)}else{jQuery('html').addClass('browserMozilla')}}if(jQuery.browser.opera){jQuery('html').addClass('browserOpera')}

});




